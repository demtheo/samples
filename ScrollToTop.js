import React from 'react'
import './ScrollToTop.css'

export default class ScrollToTop extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      is_visible: false
    }
  }
  componentDidMount() {
    var scrollComponent = this;
    document.addEventListener("scroll", function(e) {
      scrollComponent.toggleVisibility();
    })
  }
  toggleVisibility() {
    if (window.pageYOffset > 300) {
      this.setState({
        is_visible: true
      })
    } else {
      this.setState({
        is_visible: false
      })
    }
  }
  scrollToTop(){
    window.scrollTo({top: 0, behavior: 'smooth'});
  }
  render(){
    const isFirefox = typeof InstallTrigger !== 'undefined';
    return(
      <div>
        {this.state.is_visible && (
          <button onClick={()=> this.scrollToTop()} className={isFirefox? 'scroll-btn scroll-btn-firefox' : 'scroll-btn'}>
            <i className='fa fa-angle-up' ></i>
          </button>
        )}
      </div>
    )
  }
}
